export const pageTitle = (
    { subtitle }: { subtitle: string | undefined } = { subtitle: undefined },
) => {
    const title = 'biologyis.fun'
    if (subtitle) {
        return `${subtitle} | ${title}`
    }
    return title
}
