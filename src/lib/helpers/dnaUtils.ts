export const dnaBasePairCompliment = (char: string): string => {
    switch (char) {
        case 'A':
            return 'T';
        case 'T':
            return 'A';
        case 'C':
            return 'G';
        case 'G':
            return 'C';
        default:
            return 'X';
    }
};

export const getDnaCompliment = (sequence: string): string => {
    let str = '';
    for (let i = 0; i < sequence.length; i++) {
        str += dnaBasePairCompliment(sequence.charAt(i));
    }
    return str;
};

export const rnaBasePairTranslate = (char: string): string => {
    if (char == 'T') {
        return 'U';
    } else {
        return char;
    }
};

export const getRnaTranslation = (sequence: string): string => {
    let str = '';
    for (let i = 0; i < sequence.length; i++) {
        str += rnaBasePairTranslate(sequence.charAt(i));
    }
    return str;
};
