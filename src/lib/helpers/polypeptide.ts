export const getPolypeptide = (sequence: string) => {
    const first = sequence[0];
    const second = sequence[1];
    const third = sequence[2];

    const unknownText = 'Unknown';
    const stopText = 'Stop Codon';
    const startText = 'Start Codon';

    switch (first) {
        case 'U':
            switch (second) {
                case 'U':
                    switch (third) {
                        case 'U':
                        case 'C':
                            return 'Phenylalanine';
                        case 'A':
                        case 'G':
                            return 'Leucine';
                        default:
                            return unknownText;
                    }
                case 'C':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Serine';
                        default:
                            return unknownText;
                    }
                case 'A':
                    switch (third) {
                        case 'U':
                        case 'C':
                            return 'Tyrosine';
                        case 'A':
                        case 'G':
                            return stopText;
                        default:
                            return unknownText;
                    }
                case 'G':
                    switch (third) {
                        case 'U':
                        case 'C':
                            return 'Cysteine';
                        case 'A':
                            return stopText;
                        case 'G':
                            return 'Tryptophan';
                        default:
                            return unknownText;
                    }
                default:
                    return unknownText;
            }
        case 'C':
            switch (second) {
                case 'U':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Leucine';
                        default:
                            return unknownText;
                    }
                case 'C':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Proline';
                        default:
                            return unknownText;
                    }
                case 'A':
                    switch (third) {
                        case 'U':
                        case 'C':
                            return 'Histidine';
                        case 'A':
                        case 'G':
                            return 'Glutimine';
                        default:
                            return unknownText;
                    }
                case 'G':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Arginine';
                        default:
                            return unknownText;
                    }
                default:
                    return unknownText;
            }
        case 'A':
            switch (second) {
                case 'U':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                            return 'Isoleucine';
                        case 'G':
                            return startText;
                        default:
                            return unknownText;
                    }
                case 'C':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Threonine';
                        default:
                            return unknownText;
                    }
                case 'A':
                    switch (third) {
                        case 'U':
                        case 'C':
                            return 'Asparagine';
                        case 'A':
                        case 'G':
                            return 'Lysine';
                        default:
                            return unknownText;
                    }
                case 'G':
                    switch (third) {
                        case 'U':
                        case 'C':
                            return 'Serine';
                        case 'A':
                        case 'G':
                            return 'Arginine';
                        default:
                            return unknownText;
                    }
                default:
                    return unknownText;
            }
        case 'G':
            switch (second) {
                case 'U':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Valine';
                        default:
                            return unknownText;
                    }
                case 'C':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Alanine';
                        default:
                            return unknownText;
                    }
                case 'A':
                    switch (third) {
                        case 'U':
                        case 'C':
                            return 'Aspartic Acid';
                        case 'A':
                        case 'G':
                            return 'Glutamic Acid';
                        default:
                            return unknownText;
                    }
                case 'G':
                    switch (third) {
                        case 'U':
                        case 'C':
                        case 'A':
                        case 'G':
                            return 'Glycine';
                        default:
                            return unknownText;
                    }
                default:
                    return unknownText;
            }
        default:
            return unknownText;
    }
};
