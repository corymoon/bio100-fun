import { getPolypeptide } from './polypeptide';

export const processSequences = (sequences: string[]): string[] => {
    if (sequences) {
        const arr: string[] = [];
        sequences.forEach((s) => {
            arr.push(getPolypeptide(s));
        });
        return arr;
    } else {
        return [];
    }
};
