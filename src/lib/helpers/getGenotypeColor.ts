export const getGenotypeColor = (genotype: string, colors: { pair: string; color: string }[]) => {
    for (let i = 0; i < colors.length; i++) {
        if (colors[i].pair === genotype) {
            return colors[i].color
        }
    }
}
