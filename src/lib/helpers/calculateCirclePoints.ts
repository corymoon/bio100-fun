import _ from 'lodash'

export type Coordinate = {x: number, y: number}

const degreesToRadians = (degrees: number) => degrees * (Math.PI / 180)

export const point = (radius: number, angle: number, centerX: number, centerY: number) => {
    const x = radius * Math.cos(degreesToRadians(angle + 270)) + centerX
    const y = radius * Math.sin(degreesToRadians(angle + 270)) + centerY

    return { x, y }
}

export const getPoints = (numPoints: number, radius: number, centerX: number, centerY: number) => {
    let points: Coordinate[] = []
    const angleIncrement = (360 / numPoints)
    let _angle = angleIncrement
    for (let i = 0; i < numPoints; i++) {
        const newPoint = point(radius, _angle, centerX, centerY)
        points = [newPoint, ...points]
        _angle = _angle + angleIncrement
    }

    const arrs: Coordinate[][] = _.chunk(points, points.length / 2);

    const newPoints: Coordinate[] = []
    for (let i = 0; i < arrs[0].length; i++) {
        newPoints.push(arrs[0][i])
        newPoints.push(arrs[1][i])
    }
    
    return newPoints
}
