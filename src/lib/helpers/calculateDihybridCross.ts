import _ from 'lodash'
import { sortAlleles } from './sortAlleles'

// input -> p1 = SsYy, p2 = SsYy
export const calculateDihybridCross = (p1: string, p2: string) => {
    const p1S = p1[0],
        p1s = p1[1],
        p1Y = p1[2],
        p1y = p1[3]
    const p2S = p2[0],
        p2s = p2[1],
        p2Y = p2[2],
        p2y = p2[3]

    const cross = [
        ['', p1S + p1Y, p1S + p1y, p1s + p1Y, p1s + p1y],
        [
            p2S + p2Y,
            sortAlleles(p1S, p2S) + sortAlleles(p1Y, p2Y),
            sortAlleles(p1S, p2S) + sortAlleles(p1y, p2Y),
            sortAlleles(p1s, p2S) + sortAlleles(p1Y, p2Y),
            sortAlleles(p1s, p2S) + sortAlleles(p1y, p2Y),
        ],
        [
            p2S + p2y,
            sortAlleles(p1S, p2S) + sortAlleles(p1Y, p2y),
            sortAlleles(p1S, p2S) + sortAlleles(p1y, p2y),
            sortAlleles(p1s, p2S) + sortAlleles(p1Y, p2y),
            sortAlleles(p1s, p2S) + sortAlleles(p1y, p2y),
        ],
        [
            p2s + p2Y,
            sortAlleles(p1S, p2s) + sortAlleles(p1Y, p2Y),
            sortAlleles(p1S, p2s) + sortAlleles(p1y, p2Y),
            sortAlleles(p1s, p2s) + sortAlleles(p1Y, p2Y),
            sortAlleles(p1s, p2s) + sortAlleles(p1y, p2Y),
        ],
        [
            p2s + p2y,
            sortAlleles(p1S, p2s) + sortAlleles(p1Y, p2y),
            sortAlleles(p1S, p2s) + sortAlleles(p1y, p2y),
            sortAlleles(p1s, p2s) + sortAlleles(p1Y, p2y),
            sortAlleles(p1s, p2s) + sortAlleles(p1y, p2y),
        ],
    ]

    return cross
}

export const dihybridUniqueValues = (cross: string[][]) => _.uniq(dihybridAllValues(cross))

export const dihybridAllValues = (cross: string[][]) => {
    const values: string[] = []
    for (let i = 0; i < cross.length; i++) {
        const item = cross[i]
        for (let idx = 0; idx < item.length; idx++) {
            const genotype = item[idx]
            if (genotype.length === 4) {
                values.push(genotype)
            }
        }
    }
    return values
}
