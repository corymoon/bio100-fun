import { getPolypeptide } from './polypeptide'
import { splitSequence } from './splitSequence'
import { processSequences } from './processSequences'
import { getDnaCompliment, dnaBasePairCompliment, getRnaTranslation } from './dnaUtils'
import { sortAlleles, makeAllelePairs, mapUniqueToColors } from './sortAlleles'
import {
    calculateDihybridCross,
    dihybridUniqueValues,
    dihybridAllValues,
} from './calculateDihybridCross'
import {
    calculateMonohybridCross,
    monohybridUniqueValues,
    monohybridAllValues,
} from './calculateMonohybridCross'
import { getPhenotypeFromGenotype } from './calculatePhenotypes'
import { checkAlleles } from './checkAlleles'
import { getGenotypeColor } from './getGenotypeColor'
import { pageTitle } from './pageTitle'

export {
    pageTitle,
    getPolypeptide,
    splitSequence,
    processSequences,
    getDnaCompliment,
    dnaBasePairCompliment,
    getRnaTranslation,
    sortAlleles,
    makeAllelePairs,
    mapUniqueToColors,
    calculateDihybridCross,
    dihybridUniqueValues,
    dihybridAllValues,
    getPhenotypeFromGenotype,
    calculateMonohybridCross,
    monohybridUniqueValues,
    monohybridAllValues,
    checkAlleles,
    getGenotypeColor,
}
