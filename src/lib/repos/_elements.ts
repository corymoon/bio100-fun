export type ElementParticles = {
    name: string
    protons: number
    neutrons: number
    electrons: number
}

export const elementsByParticle: ElementParticles[] = [
    { name: 'Hydrogen', protons: 1, neutrons: 0, electrons: 1 },
    { name: 'Helium', protons: 2, neutrons: 2, electrons: 2 },
    { name: 'Lithium', protons: 3, neutrons: 4, electrons: 3 },
    { name: 'Beryllium', protons: 4, neutrons: 5, electrons: 4 },
    { name: 'Boron', protons: 5, neutrons: 6, electrons: 5 },
    { name: 'Carbon', protons: 6, neutrons: 6, electrons: 6 },
    { name: 'Nitrogen', protons: 7, neutrons: 7, electrons: 7 },
    { name: 'Oxygen', protons: 8, neutrons: 8, electrons: 8 },
    { name: 'Fluorine', protons: 9, neutrons: 10, electrons: 9 },
    { name: 'Neon', protons: 10, neutrons: 10, electrons: 10 },
    { name: 'Sodium', protons: 11, neutrons: 12, electrons: 11 },
    { name: 'Magnesium', protons: 12, neutrons: 12, electrons: 12 },
    { name: 'Aluminum', protons: 13, neutrons: 14, electrons: 13 },
    { name: 'Silicon', protons: 14, neutrons: 14, electrons: 14 },
    { name: 'Phosphorous', protons: 15, neutrons: 16, electrons: 15 },
    { name: 'Sulfur', protons: 16, neutrons: 16, electrons: 16 },
    { name: 'Chlorine', protons: 17, neutrons: 17, electrons: 17 },
    { name: 'Argon', protons: 18, neutrons: 22, electrons: 18 },
    { name: 'Potassium', protons: 19, neutrons: 20, electrons: 19 },
    { name: 'Calcium', protons: 20, neutrons: 20, electrons: 20 },
    { name: 'Scandium', protons: 21, neutrons: 24, electrons: 21 },
    { name: 'Titanium', protons: 22, neutrons: 26, electrons: 22 },
    { name: 'Vanadium', protons: 23, neutrons: 28, electrons: 23 },
    { name: 'Chromium', protons: 24, neutrons: 28, electrons: 24 },
    { name: 'Manganese', protons: 25, neutrons: 30, electrons: 25 },
    { name: 'Iron', protons: 26, neutrons: 30, electrons: 26 },
    { name: 'Cobalt', protons: 27, neutrons: 32, electrons: 27 },
    { name: 'Nickel', protons: 28, neutrons: 31, electrons: 28 },
    { name: 'Copper', protons: 29, neutrons: 35, electrons: 29 },
    { name: 'Zinc', protons: 30, neutrons: 35, electrons: 30 },
    { name: 'Gallium', protons: 31, neutrons: 39, electrons: 31 },
    { name: 'Germanium', protons: 32, neutrons: 41, electrons: 32 },
    { name: 'Arsenic', protons: 33, neutrons: 42, electrons: 33 },
    { name: 'Selenium', protons: 34, neutrons: 45, electrons: 34 },
    { name: 'Bromine', protons: 35, neutrons: 45, electrons: 35 },
    { name: 'Krypton', protons: 36, neutrons: 48, electrons: 36 },
    { name: 'Rubidium', protons: 37, neutrons: 48, electrons: 37 },
    { name: 'Strontium', protons: 38, neutrons: 50, electrons: 38 },
    { name: 'Yttrium', protons: 39, neutrons: 50, electrons: 39 },
    { name: 'Zirconium', protons: 40, neutrons: 51, electrons: 40 },
    { name: 'Niobium', protons: 41, neutrons: 52, electrons: 41 },
    { name: 'Molybdenum', protons: 42, neutrons: 54, electrons: 42 },
    { name: 'Technetium', protons: 43, neutrons: 55, electrons: 43 },
    { name: 'Ruthenium', protons: 44, neutrons: 57, electrons: 44 },
    { name: 'Rhodium', protons: 45, neutrons: 58, electrons: 45 },
    { name: 'Palladium', protons: 46, neutrons: 60, electrons: 46 },
    { name: 'Silver', protons: 47, neutrons: 61, electrons: 47 },
    { name: 'Cadmium', protons: 48, neutrons: 64, electrons: 48 },
    { name: 'Indium', protons: 49, neutrons: 66, electrons: 49 },
    { name: 'Tin', protons: 50, neutrons: 69, electrons: 50 },
    { name: 'Antimony', protons: 51, neutrons: 71, electrons: 51 },
    { name: 'Tellurium', protons: 52, neutrons: 76, electrons: 52 },
    { name: 'Iodine', protons: 53, neutrons: 74, electrons: 53 },
    { name: 'Xenon', protons: 54, neutrons: 77, electrons: 54 },
    { name: 'Cesium', protons: 55, neutrons: 78, electrons: 55 },
    { name: 'Barium', protons: 56, neutrons: 81, electrons: 56 },
    { name: 'Lanthanum', protons: 57, neutrons: 82, electrons: 57 },
    { name: 'Cerium', protons: 58, neutrons: 82, electrons: 58 },
    { name: 'Praseodymium', protons: 59, neutrons: 82, electrons: 59 },
    { name: 'Neodymium', protons: 60, neutrons: 84, electrons: 60 },
    { name: 'Promethium', protons: 61, neutrons: 84, electrons: 61 },
    { name: 'Samarium', protons: 62, neutrons: 88, electrons: 62 },
    { name: 'Europium', protons: 63, neutrons: 89, electrons: 63 },
    { name: 'Gadolinium', protons: 64, neutrons: 93, electrons: 64 },
    { name: 'Terbium', protons: 65, neutrons: 94, electrons: 65 },
    { name: 'Dysprosium', protons: 66, neutrons: 97, electrons: 66 },
    { name: 'Holmium', protons: 67, neutrons: 98, electrons: 67 },
    { name: 'Erbium', protons: 68, neutrons: 99, electrons: 68 },
    { name: 'Thulium', protons: 69, neutrons: 100, electrons: 69 },
    { name: 'Ytterbium', protons: 70, neutrons: 103, electrons: 70 },
    { name: 'Lutetium', protons: 71, neutrons: 104, electrons: 71 },
    { name: 'Hafnium', protons: 72, neutrons: 106, electrons: 72 },
    { name: 'Tantalum', protons: 73, neutrons: 108, electrons: 73 },
    { name: 'Tungsten', protons: 74, neutrons: 110, electrons: 74 },
    { name: 'Rhenium', protons: 75, neutrons: 111, electrons: 75 },
    { name: 'Osmium', protons: 76, neutrons: 114, electrons: 76 },
    { name: 'Iridium', protons: 77, neutrons: 115, electrons: 77 },
    { name: 'Platinum', protons: 78, neutrons: 117, electrons: 78 },
    { name: 'Gold', protons: 79, neutrons: 118, electrons: 79 },
    { name: 'Mercury', protons: 80, neutrons: 121, electrons: 80 },
    { name: 'Thallium', protons: 81, neutrons: 123, electrons: 81 },
    { name: 'Lead', protons: 82, neutrons: 125, electrons: 82 },
    { name: 'Bismuth', protons: 83, neutrons: 126, electrons: 83 },
    { name: 'Polonium', protons: 84, neutrons: 125, electrons: 84 },
    { name: 'Astatine', protons: 85, neutrons: 125, electrons: 85 },
    { name: 'Radon', protons: 86, neutrons: 136, electrons: 86 },
    { name: 'Francium', protons: 87, neutrons: 136, electrons: 87 },
    { name: 'Radium', protons: 88, neutrons: 138, electrons: 88 },
    { name: 'Actinium', protons: 89, neutrons: 138, electrons: 89 },
    { name: 'Thorium', protons: 90, neutrons: 142, electrons: 90 },
    { name: 'Protactinium', protons: 91, neutrons: 148, electrons: 91 },
    { name: 'Uranium', protons: 92, neutrons: 146, electrons: 92 },
    { name: 'Neptunium', protons: 93, neutrons: 144, electrons: 93 },
    { name: 'Plutonium', protons: 94, neutrons: 150, electrons: 94 },
    { name: 'Americium', protons: 95, neutrons: 148, electrons: 95 },
    { name: 'Curium', protons: 96, neutrons: 151, electrons: 96 },
    { name: 'Berkelium', protons: 97, neutrons: 150, electrons: 97 },
    { name: 'Californium', protons: 98, neutrons: 153, electrons: 98 },
    { name: 'Einsteinium', protons: 99, neutrons: 153, electrons: 99 },
    { name: 'Fermium', protons: 100, neutrons: 157, electrons: 100 },
    { name: 'Mendelevium', protons: 101, neutrons: 157, electrons: 101 },
    { name: 'Nobelium', protons: 102, neutrons: 157, electrons: 102 },
    { name: 'Lawrencium', protons: 103, neutrons: 159, electrons: 103 },
    { name: 'Rutherfordium', protons: 104, neutrons: 157, electrons: 104 },
    { name: 'Dubnium', protons: 105, neutrons: 157, electrons: 105 },
    { name: 'Seaborgium', protons: 106, neutrons: 157, electrons: 106 },
    { name: 'Bohrium', protons: 107, neutrons: 155, electrons: 107 },
    { name: 'Hassium', protons: 108, neutrons: 157, electrons: 108 },
    { name: 'Meitnerium', protons: 109, neutrons: 157, electrons: 109 },
    { name: 'Darmstadtium', protons: 110, neutrons: 151, electrons: 110 },
    { name: 'Roentgenium', protons: 111, neutrons: 161, electrons: 111 },
    { name: 'Copernicium', neutrons: 165, protons: 112, electrons: 112 },
    { name: 'Nihonium', neutrons: 173, protons: 113, electrons: 113 },
    { name: 'Flerovium', neutrons: 175, protons: 114, electrons: 114 },
    { name: 'Moscovium', neutrons: 174, protons: 115, electrons: 115 },
    { name: 'Livermorium', neutrons: 177, protons: 116, electrons: 116 },
    { name: 'Tennessine', neutrons: 177, protons: 117, electrons: 117 },
    { name: 'Tennessine', neutrons: 176, protons: 118, electrons: 118 },
]
