import { allElements, type ElementInfo } from './allElements'
import _ from 'lodash'
import { elementsByParticle, type ElementParticles } from './_elements'

class ElementRepo {
    getAll = (): ElementInfo[] => {
        return allElements
    }

    getElementByName = (keyword: string): ElementInfo => {
        return _.find(allElements, { name: keyword })
    }

    getElementBySymbol = (symbol: string): ElementInfo => {
        return _.find(allElements, { symbol })
    }

    getElementByAtomicNumber = (atomicNumber: number): ElementInfo => {
        return _.find(allElements, { atomicNumber })
    }

    getElementParticles = (atomicNumber: number): ElementParticles => {
        return elementsByParticle[atomicNumber - 1]
    }
}

export const elementRepo = new ElementRepo()
