import type { learningSection } from 'src/types'

export const navSections: learningSection[] = [
    { title: 'Scientific Thinking', links: [] },
    {
        title: 'The Chemistry of Biology',
        links: [
            {
                label: 'Atoms',
                href: '/atoms',
                info: 'to interactively explore the atoms of the Periodic Table',
            },
        ],
    },
    { title: 'Molecules of Life', links: [] },
    { title: 'Cells', links: [] },
    { title: 'Energy', links: [] },
    {
        title: 'DNA and Gene Expression',
        links: [
            {
                label: 'DNA/RNA Transcription and Translation',
                href: '/dna-rna',
                info: 'to transcribe and translate any DNA sequence',
            },
        ],
    },
    { title: 'Biotechnology', links: [] },
    { title: 'Chromosomes and Cell Division', links: [] },
    {
        title: 'Genes and Inheritance',
        links: [
            {
                label: 'Mendelian Genetics',
                href: '/mendelian-genetics',
                info: 'to make monohybrid and dihybrid crosses of different phenotypes',
            },
        ],
    },
    { title: 'Evolution and Natural Selection', links: [] },
    { title: 'Evolution and Behavior', links: [] },
    { title: 'The Origin and Diversification of Life on Earth', links: [] },
    { title: 'Animal Diversification', links: [] },
    { title: 'Plant and Fungi Diversification', links: [] },
    { title: 'Microbe Diversification', links: [] },
    { title: 'Population Ecology', links: [] },
    { title: 'Ecosystems and Communities', links: [] },
    { title: 'Conservation and Biodiversity', links: [] },
    { title: 'Plant Structure and Nutrient Transport', links: [] },
    { title: 'Growth, Reproduction, and Environmental Responses in Plants', links: [] },
    { title: 'Introduction to Animal Physiology', links: [] },
    { title: 'Circulation and Respiration', links: [] },
    { title: 'Nutrition and Digestion', links: [] },
    { title: 'Nervous and Motor Systems', links: [] },
    { title: 'Hormones', links: [] },
    { title: 'Reproduction and Development', links: [] },
    { title: 'Immunity and Health', links: [] },
]
