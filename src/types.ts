export type navItem = { label: string; href: string }
export type sectionItem = { label: string; href: string; info: string | undefined }
export type learningSection = { title: string; links: sectionItem[] }
export type crossType = 'mono' | 'di'

export type ArrayLengthMutationKeys = 'splice' | 'push' | 'pop' | 'shift' | 'unshift' | number
export type ArrayItems<T extends Array<unknown>> = T extends Array<infer TItems> ? TItems : never
export type FixedLengthArray<T extends unknown[]> = Pick<
    T,
    Exclude<keyof T, ArrayLengthMutationKeys>
> & {
    [Symbol.iterator]: () => IterableIterator<ArrayItems<T>>
}

export type genotype = FixedLengthArray<[string, string]>
